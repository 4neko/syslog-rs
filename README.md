# This is an issues tracking repository!!!
# The source code is available at: https://repo.4neko.org/4NEKO/syslog-rs

# syslog-rs ![KPI logo](https://gitlab.com/4neko/syslog-rs/-/raw/master/logo_600.png?ref_type=heads&inline=true)

This crate is still under development. 
v 0.6

An implementation of the syslog from glibc/libc like it was designed in
in both system libraries. The API is almost compatible with what is in
libc/glibc.

Available features:
- feature = "use_async" for asynchronious code
- feature = "use_sync" for synchronious code
- feature = "use_sync_queue" for synchronious with async processing
- feature = "build_with_net" enables the TCP/UDP and extended interface

The `use_sync` is acting like the libc's/glibc's functions syslog(), openlog()...

The `use_sync_queue` has the same API as libc/glibc but is different in some ways. Also, it spawns a worker thread which sends messages from the queue to syslog.

The `use_async` is async realization of the `use_sync`. Untested, probably requires further dev.

All 3 features can be used simpltaniously. 

Available tunables:
- feature = "udp_truncate_1024_bytes"
- feature = "udp_truncate_1440_bytes" DEFAULT

The above is for RFC5424 which controls the syslog message length for forwarding via UDP protocol.

- feature = "tcp_truncate_1024_bytes"
- feature = "tcp_truncate_2048_bytes" DEFAULT
- feature = "tcp_truncate_4096_bytes"
- feature = "tcp_truncate_max_bytes"

The above is for RFC5424 which controls the syslog message length for forwarding via TCP protocol.

- feature = "dgram_sysctl_failure_panic"

The above is for *BSD systems only and controls the behaviour of the sysctl error handling. If this is
enabled, the crate will panic is access to sysctl fails. Not enabled by default.

!!! `use_async` is using tokio mutex to achieve the synchronization. On the large async queues, when many tasks are spawned, the syslog becomes a performace bottleneck because syslog server may be busy, and syslog() is slow, and calling syslog() will lock other tasks until the lock will be released. Maybe it is good idea to have for example for each tokio thread a sibgle instance of the syslog.


## Usage:

For customization:
syslog-rs = {version = "0.6", default-features = false, features = ["use_sync"]}

## Supports

- GNU/Linux RFC3164 (UTF-8 by default)
- *BSD and OSX RFC5424 (BOM UTF-8 by default)

## Contributors

Ordered by Relkom s.r.o (c) 2021

Developed by:
Aleksandr Morozov 

## Example

```rust
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate syslog_rs;

use std::thread;
use std::time::Duration;
use syslog_rs::sy_sync::{Syslog, SyslogStd};
use syslog_rs::{LogStat, LogFacility, Priority};


lazy_static! {
    static ref SYNC_SYSLOG: UnsafeReadOnlyCell<Syslog> = 
        unsafe { UnsafeReadOnlyCell::new_uninitialized("syslog_sync") };
}

macro_rules! logdebug 
{
    ($($arg:tt)*) => (
        SYSLOG.syslog(Priority::LOG_DEBUG, format!($($arg)*))
    )
}



fn main()
{
    let syslog = 
        Syslog::openlog(
            Some("example"), 
            LogStat::LOG_CONS | LogStat::LOG_NDELAY | LogStat::LOG_PID, 
            LogFacility::LOG_DAEMON
        ).unwrap();

    unsafe { SYNC_SYSLOG.init(syslog) };
    
    logdebug!("test message!");

    thread::sleep(Duration::from_micros(10));

    return;
}
```

```rust
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate syslog_rs;

use std::thread;
use std::time::Duration;

#[cfg(feature = "use_sync")]
use syslog_rs::sy_sync::{Syslog, SyslogStd};

use syslog_rs::{LogStat, LogFacility, Priority};

lazy_static! {
    static ref SYSLOG: Syslog = 
        Syslog::openlog(
            Some("example"), 
            LogStat::LOG_CONS | LogStat::LOG_NDELAY | LogStat::LOG_PID, 
            LogFacility::LOG_DAEMON
        ).unwrap();
}

macro_rules! logdebug 
{
    ($($arg:tt)*) => (
        SYSLOG.syslog(Priority::LOG_DEBUG, format!($($arg)*))
    )
}

pub fn main()
{
    logdebug!("test message!");

    thread::sleep(Duration::from_micros(10));

    return;
}
```

## Benchmarking

The test spawns 2 threads and one main thread. All 3 threads are sending messages to syslog. The time measurment in the tables are approximations.

Results of the tests in syslog_*.rs files in Debug mode (AMD Ryzen 5 7600X 6-Core Processor):

| use_sync (sys mutex) | use_sync_queue |   use_async  |
|----------            |----------------|--------------|
|main: 81.101µs       | main: 12.74µs   |main: 67.549µs|
|t1: 91.02µs          |t2: 1.77µs   |t2: 44.779µs   |
|t2: 129.919µs          |t1: 4.49µs      |t1: 5.76µs  |
|t1: 31.32µs          |t2: 630ns  |t1: 7.74µs   |
|t2: 9.09µs          |t1: 320ns    |t2: 4.58µs  |
|t1: 10.82µs         |t2: 1.13µs      |t2: 13.77µs   |
|t2: 7.23µs         |t1: 280ns      |t1: 5.62µs   |
|t1: 11.68µs          |t1: 950ns    |t1: 6.55µs   |
|t2: 7.239µs          |t2: 750ns      |t2: 4.47µs  |
|t1: 11.57µs          |t1: 720ns   |t2: 7.22µs   |
|t2: 6.61µs          |t2: 700ns    |t1: 4.43µs   |



